module gitlab.com/etke.cc/tools/ttm

go 1.17

require (
	github.com/creack/pty v1.1.21
	gitlab.com/etke.cc/go/env v1.1.0
)
